# Gulp template
### Author: @teqst

It is just a simple gulp template for making web pages

Contains:
* Pug template support
* SCSS support
* Processing fonts and images
* File watcher
* Browser sync plugin
* Filinclude plugin